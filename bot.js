const sdk = require('matrix-js-sdk');

/*******************************************************************************
* Matrix connect
*******************************************************************************/

const myUserId = '@solarus-welcome:matrix.org'

var client = sdk.createClient({
  baseUrl: 'https://matrix.org',
  accessToken: process.env['MATRIX_ACCESS_TOKEN'],
  userId: myUserId
})

client.on("RoomMember.membership", function(event, member) {
  // Automatically join rooms when invited
  if (member.membership === "invite" && member.userId === myUserId) {
    client.joinRoom(member.roomId).catch(function() {}).done(function() {
      console.log("Auto-joined %s", member.roomId);
    });
  }

  // Greet new users
  if (member.membership === "join" && member.userId != myUserId && event.event.unsigned.age < 6000) {
    let roomId = member.roomId;
    // 2 second delay in messages so it shows bot "typing"
    const typingTime = 2000;
    client.sendTyping(roomId, true, typingTime, function() {
      setTimeout(function() {
        client.sendMessage(roomId, {
          "msgtype": "m.text",
          "format": "org.matrix.custom.html",
          "body": `Welcome ${member.rawDisplayName}!\n\nIf you have a question, please search the Solarus Forum first: http://forum.solarus-games.org/\n\nIf you're curious whether Solarus is capable of something, I recommend checking out the scripts section: http://forum.solarus-games.org/index.php/board,9.0.html\n\nIn general, Solarus can do just about anything. Good luck with your project, and don't hesitate to reach out if you need technical advice!`,
          "formatted_body": `Welcome <a href=\"https://matrix.to/#/${member.userId}\">${member.rawDisplayName}</a>!<br><br>If you have a question, please search the Solarus Forum first: http://forum.solarus-games.org/<br><br>If you're curious whether Solarus is capable of something, I recommend checking out the scripts section: http://forum.solarus-games.org/index.php/board,9.0.html<br><br>In general, Solarus can do just about anything. Good luck with your project, and don't hesitate to reach out if you need technical advice!`
        });
        client.sendTyping(roomId, false);
      }, typingTime);
      return;
    })
  }
});

client.startClient({initialSyncLimit: 0});
